from flask import Flask
from flask_restful import Api
from resources import Index, Securelink

app = Flask(__name__)
api = Api(app)
# app.config['SERVER_NAME'] = '127.0.0.1:5000'

api.add_resource(Index, '/')
api.add_resource(Securelink, '/securelink')

if __name__ == "__main__":
    app.run(debug=True)
