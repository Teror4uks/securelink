import hashlib
import base64
from flask_restful import reqparse, Resource
from flask import request

class Index(Resource):
    def get(self):
        return 200

class Securelink(Resource):
    def __init__(self, *args, **kwargs):
        self.parser = reqparse.RequestParser()
        super().__init__(*args, **kwargs)

    def get(self):
        params = ('t', 'u', 'ip', 'p')
        for p in params:
            self.parser.add_argument(p)
        
        if not self.check_secure_link_params(self.parser.parse_args()):
            return 400

        args = self.parser.parse_args()
        ip, timestamp, url, paswd = args['ip'], args['t'], args['u'], args['p']

        # echo -n '2147483647/s/link127.0.0.1 secret' | openssl md5 -binary | openssl base64 | tr +/ -_ | tr -d =
        # /s/link?md5=_e4Nc3iduzkWRm01TBBNYw&expires=2147483647
        _temp = "{timestamp}/{url}{ip}={paswd}".format(timestamp=timestamp,url=url,ip=ip,paswd=paswd)
        _temp = _temp.encode('ascii')

        _hash = hashlib.md5()
        _hash.update(_temp)

        md5_hash = base64.b64encode(_hash.digest()).decode("utf-8")
        md5_hash = self.format_uri(md5_hash)

        rel_uri = "{url}?md5={md5_hash}&expires={timestamp}".format(url=url, md5_hash=md5_hash, timestamp=timestamp)
        abs_uri = "{}{}".format(request.url_root,rel_uri)
    
        return abs_uri

    @staticmethod
    def get_md5(s):
        hash = hashlib.md5()
        return hash.digest().encode('base64')

    @staticmethod
    def format_uri(uri):
        uri = uri.replace("+", "-")
        uri = uri.replace("/", "_")
        uri = uri.replace("=", "")
        return uri


    @staticmethod
    def check_secure_link_params(parametrs):
        return all(param is not None for param in parametrs.values())
